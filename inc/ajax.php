<?php

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('ajax-script', PATH_PLUGIN_MA . 'assets/js/ajax.js', array('jquery'), '', true);
    wp_localize_script('ajax-script', 'getcode_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
});
add_action('wp_ajax_getcode_ajax_call', 'enroll_getcode');
add_action('wp_ajax_nopriv_getcode_ajax_call', 'enroll_getcode');

function enroll_getcode()
{
    $idSite = get_option('option_site_id');
    //$idSite = 4;
    $api = get_option('option_site_getma') . "?id=$idSite&ip=" . get_the_user_ip();
    //$api = "https://ma.alopie.com?id=$idSite&ip=" . get_the_user_ip();
    $decode = json_decode(file_get_contents($api), true);
    if (isset($decode['status']) && $decode['status'] == true) {
        $code = $decode['code'];
        echo "Mã xác nhận: $code";
    } else {
        echo $decode['mess'];
    }

    exit();
}

