<?php
add_action( 'admin_menu', function (){
    add_menu_page(
        'Cấu hình get mã',
        'Cấu hình get mã',
        'manage_options',
        'getma-slug',
        'getma_page_content',
        'dashicons-admin-network',
        100
    );
} );



function getma_page_content(){

    echo '<div class="wrap">
	<h1>Cấu hình get mã</h1>
	<form method="post" action="options.php">';

    settings_fields( 'getma_settings' ); // settings group name
    do_settings_sections( 'getma-slug' ); // just a page slug
    submit_button();

    echo '</form></div>';

}

add_action( 'admin_init',  function (){
    register_setting(
        'getma_settings', // settings group name
        'option_site_getma', // option name
        'sanitize_text_field' // sanitization function
    );
    register_setting(
        'getma_settings', // settings group name
        'option_site_id', // option name
        'sanitize_text_field' // sanitization function
    );
    register_setting(
        'getma_settings', // settings group name
        'option_time_delay', // option name
        'sanitize_text_field' // sanitization function
    );


    add_settings_section(
        'some_settings_section_id', // section ID
        '', // title (if needed)
        '', // callback function (if needed)
        'getma-slug' // page slug
    );

    add_settings_field(
        'homepage_text',
        'Cấu hình site API',
        'getma_text_field_html', // function which prints the field
        'getma-slug', // page slug
        'some_settings_section_id' // section ID
    );
    add_settings_field(
        'id_site_text',
        'Cấu hình ID SITE',
        'getma_id_field_html', // function which prints the field
        'getma-slug', // page slug
        'some_settings_section_id'// section ID
    );

    add_settings_field(
        'time_delay_text',
        'Cấu hình thời gian hiện mã',
        'getma_time_delay_field_html', // function which prints the field
        'getma-slug', // page slug
        'some_settings_section_id'// section ID
    );
} );


function getma_text_field_html(){

    $site = empty($site=get_option( 'option_site_getma' ))?'https://ma.alopie.com':$site;

    printf(
        '<input type="text" id="option_site_getma" name="option_site_getma" value="%s" />',
        esc_attr( $site )
    );

}
function getma_id_field_html(){
    $id = empty($id=get_option( 'option_site_id' ))?1:$id;
    printf(
        '<input type="text" id="option_site_id" name="option_site_id" value="%s" />',
        esc_attr( $id )
    );
}
function getma_time_delay_field_html(){
    $time = get_option( 'option_time_delay' );
    $time = empty($time=get_option( 'option_time_delay' ))?20:$time;
    printf(
        '<input type="text" id="option_time_delay" name="option_time_delay" value="%s" />',
        esc_attr( $time )
    );
}