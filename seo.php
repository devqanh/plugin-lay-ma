<?php
/**
 * Plugin Name: SEO GET MÃ
 * Plugin URI:  https://dev.org
 * Description: SEO GET MÃ shortcode [get-ma-seo]
 * Version:     1.6.2
 * Author:      WordPress Contributors
 */
define("PATH_PLUGIN_MA", plugin_dir_url(__FILE__));
require_once 'inc/options.php';
require_once 'inc/ajax.php';

add_shortcode('get-ma-seo', function () {

    $time_delay = 0;

    $ref = getallheaders()['Referer'];

    if (isset($ref) && strpos($ref, 'google') == false || empty($ref)) {

        $mess = "Vui lòng tìm kiếm từ khóa theo hướng dẫn để nhận mã!";
    } else {
        $time_delay = get_option('option_time_delay');
        $mess = 'Mã xác nhận: <span id="countdowntimer">' . $time_delay . ' </span> giây';
    }

    ob_start();
    ?>
    <div class="avc_visit_counter_front_simple"
         style="color:red;text-align: center; width: 100%; max-width: 300px; border: 2px solid #9b0808; background-color:#ffffff; border-radius: 1px; margin: 0px auto; padding: 10px;font-weight: bold;font-size: 20px">
        <p id="text-key" style="margin: auto" time-delay="<?= $time_delay; ?>"><?= $mess; ?></p>
    </div>


    <?php
    return ob_get_clean();
});
function get_the_user_ip()
{

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {


        $ip = $_SERVER['HTTP_CLIENT_IP'];

    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {


        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

    } else {

        $ip = $_SERVER['REMOTE_ADDR'];

    }

    return apply_filters('wpb_get_ip', $ip);

}