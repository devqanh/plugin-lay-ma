jQuery(document).ready(function ($) {
    var timeleft = $('#text-key').attr('time-delay');
    if(timeleft != 0){
        var downloadTimer = setInterval(function () {
            timeleft--;
            $('#countdowntimer').text(timeleft);
            if(timeleft===8){
                $.ajax({
                    type: "get",
                    dataType: "html",
                    url: getcode_ajax_object.ajax_url,
                    data: {action:'getcode_ajax_call'},
                    success: function(msg){
                        $("#text-key").attr("mess",msg);
                    }
                });
            }
            if (timeleft === 0) {
                clearInterval(downloadTimer);
                $("#text-key").html($("#text-key").attr("mess"))
            }

        }, 1000);
    }



})